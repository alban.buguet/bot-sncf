const LAPS = 100;

const params = {
	HEURE: "10:29",
	ROOM: "BAS", //ANY, HAUT, BAS
	SEAT: "FEN", //ANY FEN, COU
};


window.addEventListener('load', function () {
	let section = window.location.href.match(/sncf\/[a-z]+\//)[0].slice(5,-1);
	if (section == "proposition"){
		seekTrain();
	}else if (section == "commande"){
		console.log("on reserve la place");
		continueNoInsurance();
	}
});


function seekTrain(){
	let train;
	setTimeout(function(){
		let horaires = document.getElementsByTagName("time");

		for(const e of horaires){
			if (e.getAttribute('datetime') == params.HEURE){
				train = e.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
				break;
			}
		}
		if (train){
			train.click();
			let select_travel;
			selectTravel();
			
		}else{
			setTimeout(seekTrain, LAPS);
		}
	}, 0);
}

function selectTravel(){
	setTimeout(function(){
		let buttons = document.getElementsByTagName('button');
		for (const b of buttons){
			if (b.getAttribute("data-auto") == "BTN_SELECT_TRAVEL"){
				select_travel = b;
				break;
			}
		}
		if(select_travel){
			select_travel.click();
			selectRoom();
		}else{
			setTimeout(selectTravel, LAPS);
		}
	}, 0);
}

function selectRoom(){
	setTimeout(function(){
		let labels = document.getElementsByTagName("label");
		let radio;
		for (const r of labels){
			if (r.getAttribute("data-auto") == "RADIO_SEATING_ROOM"){
				if (r.firstElementChild.value == params.ROOM)
					radio = r.firstElementChild;
			}
		}
		if (radio){
			radio.click();
			selectSeat();
		}else{
			setTimeout(selectRoom, LAPS);
		}
	}, 0);
}

function selectSeat(){
	setTimeout(function(){
		let labels = document.getElementsByTagName("label");
		let radio;
		for (const r of labels){
			if (r.getAttribute("data-auto") == "RADIO_SEATING_SEATS"){
				if (r.firstElementChild.value == params.SEAT)
					radio = r.firstElementChild;
			}
		}
		if (radio){
			radio.click();
			let select_seating;
			validateTravel();
		}else{
			setTimeout(selectSeat, LAPS);
		}
	}, 0);
}

function validateTravel(){
	setTimeout(function(){
		let buttons = document.getElementsByTagName('button');
		for (const b of buttons){
			if (b.getAttribute("data-auto") == "BTN_SEATING_VALIDATE"){
				select_seating = b;
				break;
			}
		}
		if(select_seating){
			select_seating.click();
		}else{
			setTimeout(validateTravel, LAPS);
		}
	}, 0);
}

function continueNoInsurance(){
	setTimeout(function(){
		let div = document.getElementsByClassName("insurance__btn");
		let continuer;
		if(div.length){
			continuer = div[0];
			if(continuer.hasChildNodes){
				continuer = continuer.firstElementChild;
			}
		}
		if(continuer){
			continuer.click();
			refuseServices();
		}else{
			setTimeout(continueNoInsurance, LAPS);
		}
	},0);
}

function refuseServices(){
	setTimeout(function(){
		let div = document.getElementsByClassName("css-7ybu8m");
		let continuer;
		if (div.length){
			continuer = div[0].firstElementChild;
		}
		if (continuer) {
			continuer.click();
		}else{
			setTimeout(refuseServices, LAPS);
		}
	}, 0);
}